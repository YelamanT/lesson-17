package kz.kaspi.Lesson17H.servlets;



import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import kz.kaspi.Lesson17H.Dao.BookDao;
import kz.kaspi.Lesson17H.entity.Book;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet ("/library")
public class LibraryServlet extends HttpServlet {
    private BookDao bookDao;
    public void init () {
        bookDao=new BookDao();
    }

    public LibraryServlet () {
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String title = req.getParameter("title");
        String author = req.getParameter("author");
        String publisher = req.getParameter("publisher");
        String year =  req.getParameter("year");

        Book book = new Book ();

        book.setTitle(title);
        book.setAuthor(author);
        book.setYear(Integer.parseInt(year));
        book.setPublisher(publisher);

        List<Book> list= new ArrayList<>();

        try {
            bookDao.registerBook(book);
            list= bookDao.allBooks();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        req.setAttribute("list", list);
        getServletConfig().getServletContext().getRequestDispatcher("/book/allBooks.jsp").forward(req,resp);


    }




}