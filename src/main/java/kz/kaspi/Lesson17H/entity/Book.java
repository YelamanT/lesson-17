package kz.kaspi.Lesson17H.entity;



import java.io.Serializable;


public class Book implements Serializable {


    private String title;

    private String author;

    private String publisher;

    private int pYear;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getYear() {
        return pYear;
    }

    public void setYear(int year) {
        this.pYear = year;
    }

    @Override
    public String toString() {
        return "Title of the Book: "+getTitle()+
                ", Author: "+getAuthor()+
                ", Publisher: "+getPublisher()+
                ", Published in: "+getYear();
    }
}
