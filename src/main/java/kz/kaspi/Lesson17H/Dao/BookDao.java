package kz.kaspi.Lesson17H.Dao;


import kz.kaspi.Lesson17H.entity.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDao {


    public void registerBook (Book book) throws ClassNotFoundException {
        String insert = "INSERT INTO book" +
                "  (title, author, publisher, pyear) VALUES " +
                " (?, ?, ?, ?);";

        Class.forName("org.postgresql.Driver");

        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/postgres","postgres","159753wwAss")) {
            if  (conn !=null) {
                System.out.println("Connected to database");
                PreparedStatement ps = conn.prepareStatement(insert);
                ps.setString(1, book.getTitle());
                ps.setString(2,book.getAuthor());
                ps.setString(3, book.getPublisher());
                ps.setInt(4,book.getYear());

                ps.executeUpdate();


            }
        }  catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public List<Book> allBooks () throws ClassNotFoundException {
        List <Book> list = new ArrayList();
        Class.forName("org.postgresql.Driver");
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/postgres","postgres","159753wwAss")) {
            if  (conn !=null) {
                System.out.println("Connected");

                Statement stmt = conn.createStatement();
                ResultSet rs;
                rs = stmt.executeQuery("select * from book");
                while (rs.next()) {
                    String title = rs.getString("title");
                    String author = rs.getString("author");
                    String publisher = rs.getString("publisher");
                    int year = rs.getInt("pyear");
                    Book book = new Book();
                    book.setPublisher(publisher);
                    book.setYear(year);
                    book.setAuthor(author);
                    book.setTitle(title);
                    list.add(book);

                }
            }


        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

}