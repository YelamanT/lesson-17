<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page import="kz.kaspi.Lesson17H.entity.Book" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>


<html>
<head>
    <meta charset="ISO-8859-1">
    <title>Books</title>
</head>
<h1>List of Books</h1>
<body>
<%
    List<Book> list = (ArrayList<Book>) request.getAttribute("list");
    for(Book book : list) {
        out.println("<p>"+book+"</p>");
    }
%>
</body>
</html>